import pyhsd
import time

t0 = time.time_ns()

d = pyhsd.distance('be110', 'hello')
m = pyhsd.match('be110', [
    'hello',
    'world',
], 2)

print("Finished in:", time.time_ns() - t0, "ns")

print("Distance:", d)
print("Matches:")
for match in m:
    print(match)